#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys

def line(number: int):
    """Return a string corresponding to the line for number"""
    lista = str(number)
    return lista * number


def triangle(number: int):
    cadena = ""
    """Return a string corresponding to the triangle for number"""
    if number <= 9:
        for number in range(1, number+1):
                l=line(number) #--LLamamos a la funcion line y creamos un bucle que vaya desde el 1 hasta el numero introducido
                cadena = cadena + l + "\n"
    else:
         raise ValueError
    return cadena

def main():
    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)


if __name__ == '__main__':
    main()
